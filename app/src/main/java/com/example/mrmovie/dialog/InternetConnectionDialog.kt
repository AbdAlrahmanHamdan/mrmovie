package com.example.mrmovie.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.mrmovie.R
import kotlinx.android.synthetic.main.layout_dialog.view.*

class InternetConnectionDialog : DialogFragment(), View.OnClickListener {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.layout_dialog, container, false)
        view.dialogButton.setOnClickListener(this)
        return view
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
    }

    override fun onClick(p0: View?) {
        if (p0?.id == R.id.dialogButton)
            this.dismiss()
    }
}