package com.example.mrmovie.activities

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.mrmovie.R
import com.example.mrmovie.adapters.MoviesAdapter
import com.example.mrmovie.controllers.DataController
import com.example.mrmovie.dialog.InternetConnectionDialog
import com.example.mrmovie.fragments.RecyclerViewFragment
import kotlinx.android.synthetic.main.layout_action_bar.*
import kotlinx.android.synthetic.main.layout_search_bar.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val dataController = DataController(this@MainActivity)
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        applicationTitle.text = "MR Movie"
        if ((connectivityManager.getActiveNetworkInfo() == null || !connectivityManager.getActiveNetworkInfo()!!.isConnected()) && checkFirstRun())
            InternetConnectionDialog().show(supportFragmentManager, "No Internet connection dialog")

        val fragment = RecyclerViewFragment()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentLayout, fragment)
        transaction.commit()

        searchBarText.setOnQueryTextListener(object:
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (searchBarText.query.toString().isNotEmpty()) {
                    dataController.search(searchBarText.query.toString(), onComplete = {
                        fragment.moviesAdapter = MoviesAdapter(this@MainActivity, it)
                        fragment.recyclerView?.adapter = fragment.moviesAdapter
                        val dividerItemDecoration =
                            DividerItemDecoration(this@MainActivity, DividerItemDecoration.VERTICAL)
                        dividerItemDecoration.setDrawable(resources.getDrawable(R.drawable.line_divider))
                        fragment.recyclerView?.addItemDecoration(dividerItemDecoration)
                    }, onFailure = {})
                }

                else {
                    dataController.fetchAllData (onComplete = {
                        fragment.moviesAdapter = MoviesAdapter(this@MainActivity, it)
                        fragment.recyclerView?.adapter = fragment.moviesAdapter
                        val dividerItemDecoration =
                            DividerItemDecoration(this@MainActivity, DividerItemDecoration.VERTICAL)
                        dividerItemDecoration.setDrawable(resources.getDrawable(R.drawable.line_divider))
                        fragment.recyclerView?.addItemDecoration(dividerItemDecoration)
                    }, onFailure = {})
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (searchBarText.query.toString().isNotEmpty()) {
                    dataController.search(searchBarText.query.toString(), onComplete = {
                        fragment.moviesAdapter = MoviesAdapter(this@MainActivity, it)
                        fragment.recyclerView?.adapter = fragment.moviesAdapter
                        val dividerItemDecoration =
                            DividerItemDecoration(this@MainActivity, DividerItemDecoration.VERTICAL)
                        dividerItemDecoration.setDrawable(resources.getDrawable(R.drawable.line_divider))
                        fragment.recyclerView?.addItemDecoration(dividerItemDecoration)
                    }, onFailure = {})
                }

                else {
                    dataController.fetchAllData (onComplete = {
                        fragment.moviesAdapter = MoviesAdapter(this@MainActivity, it)
                        fragment.recyclerView?.adapter = fragment.moviesAdapter
                        val dividerItemDecoration =
                            DividerItemDecoration(this@MainActivity, DividerItemDecoration.VERTICAL)
                        dividerItemDecoration.setDrawable(resources.getDrawable(R.drawable.line_divider))
                        fragment.recyclerView?.addItemDecoration(dividerItemDecoration)
                    }, onFailure = {})
                }
                return true
            }

        })
    }

    private fun checkFirstRun() : Boolean {
        val preferences = PreferenceManager.getDefaultSharedPreferences(baseContext)
        val preferencesEditor = preferences.edit()

        if (preferences.getBoolean("MRMovie", true)){
            preferencesEditor.putBoolean("MRMovie", false).apply()
            return true
        }
        return false
    }
}