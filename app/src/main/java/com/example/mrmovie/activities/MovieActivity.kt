package com.example.mrmovie.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.mrmovie.R
import com.example.mrmovie.fragments.MovieInformationRecyclerViewFragment

class MovieActivity : AppCompatActivity() {

    private var movieID: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie)
        movieID = intent.getStringExtra("ID")

        val fragment = MovieInformationRecyclerViewFragment()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.movieInformationFragmentLayout, fragment)
        transaction.commit()
    }

    public fun getMovieId() :String? {
        return movieID
    }
}