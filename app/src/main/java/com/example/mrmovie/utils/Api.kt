package com.example.mrmovie.utils

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface Api {
    @GET("shows")
    open fun getAllMovies(): retrofit2.Call<List<Any?>?>?
    @GET("shows/{id}")
    open fun getMovieData(@Path("id") movieID: String): retrofit2.Call<Any?>?
    @GET("search/shows")
    open fun search(@Query("q") Name:String): retrofit2.Call<List<Any?>?>?

    companion object {
        const val BASE_URL = "https://api.tvmaze.com/"
    }
}