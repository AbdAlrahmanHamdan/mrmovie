package com.example.mrmovie.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

//object Retrofit1 {
//    private var retrofit: Retrofit? = null
//    val retrofitInstance: Retrofit
//        get() {
//            if (retrofit == null) {
//                val cacheSize = (5 * 1024 * 1024).toLong()
//                val myCache = Cache(context.cacheDir, cacheSize)
//                val okHttpClient = OkHttpClient.Builder()
//                    .cache(myCache)
//                    .addInterceptor { chain ->
//                        var request = chain.request()
//                        request = if (hasNetwork(context)!!)
//
//                            request.newBuilder().header("Cache-Control", "public, max-age=" + 5).build()
//                        else
//                            request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build()
//
//                        chain.proceed(request)
//                    }
//                    .build()
//                val client = OkHttpClient.Builder()
//                    .connectTimeout(60, TimeUnit.SECONDS).writeTimeout(60,TimeUnit.SECONDS)
//                    .readTimeout(60, TimeUnit.SECONDS).build()
//                retrofit = retrofit2.Retrofit.Builder()
//                    .baseUrl(Api.BASE_URL)
//                    .client(client)
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .build()
//            }
//            return retrofit!!
//        }
//}


class RetrofitClientInstance {
    private var retrofit: Retrofit? = null
    fun getRetrofitInstance(context: Context): Retrofit {
        val cacheSize = (5 * 1024 * 1024).toLong()
        val myCache = Cache(context.cacheDir, cacheSize)
        val okHttpClient = OkHttpClient.Builder()
            .cache(myCache)

            .addInterceptor { chain ->
                var request = chain.request()
                request = if (hasNetwork(context)!!)
                    request.newBuilder().header("Cache-Control", "public, max-age=" + 5).build()
                else
                    request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build()
                chain.proceed(request)
            }
            .build()
        if (retrofit == null) {
            retrofit = retrofit2.Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofit!!
    }

    private fun hasNetwork(context: Context): Boolean? {
        var isConnected: Boolean? = false
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        if (activeNetwork != null && activeNetwork.isConnected)
            isConnected = true
        return isConnected
    }

    companion object {
        private var INSTANCE: RetrofitClientInstance? = null
        fun getInstance() = INSTANCE
            ?: RetrofitClientInstance().also {
                INSTANCE = it
            }
    }
}