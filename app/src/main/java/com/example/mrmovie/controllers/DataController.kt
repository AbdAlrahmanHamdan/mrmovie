package com.example.mrmovie.controllers

import android.content.Context
import com.example.mrmovie.R
import com.example.mrmovie.dataholders.MovieData
import com.example.mrmovie.dataholders.MovieInformationData
import com.example.mrmovie.dataholders.MovieInformationItemType
import com.example.mrmovie.utils.Api
import com.example.mrmovie.utils.RetrofitClientInstance
import com.google.gson.internal.LinkedTreeMap
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DataController(_context: Context) {

    var context: Context? = _context

    fun fetchAllData(onComplete:(list:List<MovieData>)->Unit, onFailure:()->Unit) {

        val movieList: ArrayList<MovieData> = ArrayList<MovieData>()
        val api = RetrofitClientInstance.getInstance().getRetrofitInstance(context!!).create(Api::class.java)
        val call: Call<List<Any?>?>? = api.getAllMovies()

        call!!.enqueue(object : Callback<List<Any?>?> {
            override fun onResponse(call: Call<List<Any?>?>, response: Response<List<Any?>?>) {
                val responseBody: List<Any?>? = response.body()
                for (show in responseBody!!.iterator()) {
                    val shows = (show as LinkedTreeMap<*, *>)
                    val id = shows["id"] as Double
                    val name = shows["name"] as String
                    val type = shows["type"] as String
                    val image = shows["image"] as LinkedTreeMap<*, *>?
                    var mediumImage = "https://static.tvmaze.com/images/no-img/no-img-portrait-text.png"
                    if (image != null) {
                        mediumImage = image["medium"] as String
                        mediumImage = mediumImage.replace("http", "https")
                    }
                    val rating = shows["rating"] as LinkedTreeMap<*, *>?
                    val averageRating = rating!!["average"]
                    var doubleAverageRating: Double? = 0.0
                    if (averageRating != null)
                        doubleAverageRating = averageRating as Double
                    movieList.add(MovieData(id, name, type, mediumImage, doubleAverageRating))
                }
                onComplete(movieList)
            }
            override fun onFailure(
                call: Call<List<Any?>?>, t: Throwable) {
                onFailure()
            }
        })
    }

    fun fetchMovieData(movieID: String, onComplete: (list: List<MovieInformationData>)->Unit, onFailure: () -> Unit) {
        val movieList: ArrayList<MovieInformationData> = ArrayList<MovieInformationData>()
        val api = RetrofitClientInstance.getInstance().getRetrofitInstance(context!!).create(Api::class.java)
        val call: Call<Any?>? = api.getMovieData(movieID)

        call!!.enqueue(object : Callback<Any?>{
            override fun onFailure(call: Call<Any?>, t: Throwable) {
                onFailure()
            }

            override fun onResponse(call: Call<Any?>, response: Response<Any?>) {
                val responseBody: LinkedTreeMap<*, *>? = response.body() as LinkedTreeMap<*, *>?
                val image : LinkedTreeMap<*, *>? = responseBody?.get("image") as LinkedTreeMap<*, *>?
                var originalImage = "https://static.tvmaze.com/images/no-img/no-img-portrait-text.png"
                if (image != null) {
                    originalImage = image["original"] as String
                    originalImage = originalImage.replace("http", "https")
                }
                val name = responseBody!!["name"].toString()
                val type = responseBody["type"].toString()
                val rating = responseBody["rating"] as LinkedTreeMap<*, *>?
                val averageRating = rating!!["average"]
                var doubleAverageRating: String? = "0.0"
                if (averageRating != null)
                    doubleAverageRating = (averageRating as Double).toString()

                val geners = responseBody["genres"] as ArrayList<*>
                var genersType = "undefined"
                if (geners.size != 0)
                    genersType = geners.joinToString()

                val schedule = responseBody["schedule"] as LinkedTreeMap<*, *>?
                val time = schedule!!["time"].toString()

                val daysArray = schedule["days"] as ArrayList<*>

                val days = daysArray.joinToString ()

                val status = responseBody["status"] as String
                movieList.add(MovieInformationData(text = "COVER PHOTO", image = null, type = MovieInformationItemType.CellName))
                movieList.add(MovieInformationData(text = null, image = originalImage, type = MovieInformationItemType.MovieCover))
                movieList.add(MovieInformationData(text = "NAME", image = null, type = MovieInformationItemType.CellName))
                movieList.add(MovieInformationData(text = name, image = R.drawable.ic_baseline_turned_in_24, type = MovieInformationItemType.MovieElementInfo))
                movieList.add(MovieInformationData(text = "TYPE", image = null, type = MovieInformationItemType.CellName))
                movieList.add(MovieInformationData(text = type, image = R.drawable.ic_baseline_video_library_24, type = MovieInformationItemType.MovieElementInfo))
                movieList.add(MovieInformationData(text = "SCORE", image = null, type = MovieInformationItemType.CellName))
                movieList.add(MovieInformationData(text = doubleAverageRating, image = R.drawable.ic_baseline_star_24, type = MovieInformationItemType.MovieElementInfo))
                movieList.add(MovieInformationData(text = "GENRES", image = null, type = MovieInformationItemType.CellName))
                movieList.add(MovieInformationData(text = genersType, image = R.drawable.ic_baseline_videocam_24, type = MovieInformationItemType.MovieElementInfo))
                movieList.add(MovieInformationData(text = "STATUS", image = null, type = MovieInformationItemType.CellName))

                if (status.equals("Running", true))
                    movieList.add(MovieInformationData(text = status, image = R.drawable.ic_baseline_play_circle_outline_24, type = MovieInformationItemType.MovieElementInfo))
                else
                    movieList.add(MovieInformationData(text = status, image = R.drawable.ic_baseline_stop_24, type = MovieInformationItemType.MovieElementInfo))

                movieList.add(MovieInformationData(text = "SCHEDULE", image = null, type = MovieInformationItemType.CellName))
                movieList.add(MovieInformationData(text = time, image = R.drawable.ic_baseline_access_alarms_24, type = MovieInformationItemType.MovieElementInfo))
                movieList.add(MovieInformationData(text = days, image = R.drawable.ic_baseline_calendar_today_24, type = MovieInformationItemType.MovieElementInfo))
                onComplete(movieList)
            }

        })
    }

    fun search(Name: String, onComplete:(list:List<MovieData>)->Unit, onFailure:()->Unit) {

        val movieList: ArrayList<MovieData> = ArrayList<MovieData>()
        val api = RetrofitClientInstance.getInstance().getRetrofitInstance(context!!).create(Api::class.java)
        val call: Call<List<Any?>?>? = api.search(Name)

        call!!.enqueue(object : Callback<List<Any?>?> {
            override fun onResponse(call: Call<List<Any?>?>, response: Response<List<Any?>?>) {
                val responseBody: List<Any?>? = response.body()
                for (show in responseBody!!.iterator()) {
                    val shows = (show as LinkedTreeMap<*, *>)["show"] as LinkedTreeMap<*, *>
                    val id = shows["id"] as Double
                    val name = shows["name"] as String
                    val type = shows["type"] as String
                    val image = shows["image"] as LinkedTreeMap<*, *>?
                    var mediumImage = "https://static.tvmaze.com/images/no-img/no-img-portrait-text.png"
                    if (image != null) {
                        mediumImage = image["medium"] as String
                        mediumImage = mediumImage.replace("http", "https")
                    }
                    val rating = shows["rating"] as LinkedTreeMap<*, *>?
                    val averageRating = rating!!["average"]
                    var doubleAverageRating: Double? = 0.0
                    if (averageRating != null)
                        doubleAverageRating = averageRating as Double
                    movieList.add(MovieData(id, name, type, mediumImage, doubleAverageRating))
                }
                onComplete(movieList)
            }
            override fun onFailure(
                call: Call<List<Any?>?>, t: Throwable) {
                onFailure()
            }
        })
    }
}