package com.example.mrmovie.fragments

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmovie.R
import com.example.mrmovie.activities.MovieActivity
import com.example.mrmovie.adapters.MovieInformationAdapter
import com.example.mrmovie.controllers.DataController
import kotlinx.android.synthetic.main.fragment_movie_infromation_recyler_view.view.*

class MovieInformationRecyclerViewFragment() : Fragment(){
    private var recyclerView: RecyclerView? = null
    private var movieInformationAdapter: MovieInformationAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_movie_infromation_recyler_view, container, false)
        val dataController = DataController(this.context!!)
        dataController.fetchMovieData((activity as MovieActivity?)!!.getMovieId()!!, onComplete = {
            recyclerView = root.movieInformation
            recyclerView?.setHasFixedSize(true)
            movieInformationAdapter = MovieInformationAdapter (context!!, it)
            recyclerView?.adapter = movieInformationAdapter
            recyclerView?.layoutManager = LinearLayoutManager(context)
            val dividerItemDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            dividerItemDecoration.setDrawable(resources.getDrawable(R.drawable.line_divider))
            recyclerView?.addItemDecoration(dividerItemDecoration)
        },onFailure = {})

        return root
    }
}