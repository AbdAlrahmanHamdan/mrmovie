package com.example.mrmovie.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmovie.R
import com.example.mrmovie.adapters.MoviesAdapter
import com.example.mrmovie.controllers.DataController
import kotlinx.android.synthetic.main.fragment_recycler_view.*
import kotlinx.android.synthetic.main.fragment_recycler_view.view.*

class RecyclerViewFragment : Fragment(){

    var recyclerView: RecyclerView? = null
    var moviesAdapter: MoviesAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val root = inflater.inflate(R.layout.fragment_recycler_view, container, false)

        val dataController = DataController(this.context!!)
        dataController.fetchAllData(onComplete = {
            recyclerView = root.recyclerView
            recyclerView?.setHasFixedSize(true)
            shimmerLayout.startShimmerAnimation()
            moviesAdapter = MoviesAdapter(context!!, it)
            shimmerLayout.stopShimmerAnimation()
            shimmerLayout.visibility = View.GONE
            recyclerView?.adapter = moviesAdapter
            recyclerView?.layoutManager = LinearLayoutManager(context)
            val dividerItemDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            dividerItemDecoration.setDrawable(resources.getDrawable(R.drawable.line_divider))
            recyclerView?.addItemDecoration(dividerItemDecoration)
        },onFailure = {})

        return root
    }

}