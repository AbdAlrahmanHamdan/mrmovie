package com.example.mrmovie.dataholders

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MovieData(val id: Double, val name: String, val type: String, val url: String, val rating: Double? = null) : Parcelable