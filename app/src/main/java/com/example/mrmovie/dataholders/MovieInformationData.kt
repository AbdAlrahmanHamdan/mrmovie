package com.example.mrmovie.dataholders

data class MovieInformationData(val text: String? = null, val image: Any? = null, val type: MovieInformationItemType)