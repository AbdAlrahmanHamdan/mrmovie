package com.example.mrmovie.dataholders

enum class MovieInformationItemType {
    MovieCover, MovieElementInfo, CellName
}