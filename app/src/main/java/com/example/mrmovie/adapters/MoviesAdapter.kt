package com.example.mrmovie.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmovie.R
import com.example.mrmovie.activities.MovieActivity
import com.example.mrmovie.dataholders.MovieData
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_container.view.*

class MoviesAdapter(var context: Context, var arrayList: List<MovieData?>?) : RecyclerView.Adapter<MoviesAdapter.ItemHolder> () {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val itemHolder = LayoutInflater.from(parent.context).inflate(R.layout.item_container, parent, false)
        itemHolder.setOnClickListener{
            val intent = Intent(context, MovieActivity::class.java)
            intent.putExtra("ID", itemHolder.itemName.tag.toString())
            context.startActivity(intent)
        }
        return ItemHolder(itemHolder)
    }

    override fun getItemCount(): Int {
        return arrayList!!.size
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val item: MovieData? = arrayList?.get(position)
        val picasso = Picasso.get()
        picasso.load(item!!.url).into(holder.image)
        holder.name.text = item.name
        holder.type.text = item.type
        holder.rating.rating = item.rating!!.toFloat() / 2
        holder.name.tag = item.id.toInt()
    }

    class ItemHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.itemName
        val type = itemView.itemType
        val image = itemView.itemImage
        val rating = itemView.itemRating
    }
}