package com.example.mrmovie.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmovie.R
import com.example.mrmovie.dataholders.MovieInformationData
import com.example.mrmovie.dataholders.MovieInformationItemType
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_container.view.*
import kotlinx.android.synthetic.main.layout_movie_element_info.view.*

class MovieInformationAdapter(var context: Context, var arrayList: List<MovieInformationData?>?) : RecyclerView.Adapter<MovieInformationAdapter.ItemHolder> () {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieInformationAdapter.ItemHolder {
        val itemHolder: View?
        if (viewType == 1)
            itemHolder = LayoutInflater.from(parent.context).inflate(R.layout.layout_cell_name, parent, false)
        else if (viewType == 2)
            itemHolder = LayoutInflater.from(parent.context).inflate(R.layout.layout_movie_cover, parent, false)
        else {
            itemHolder = LayoutInflater.from(parent.context).inflate(R.layout.layout_movie_element_info, parent, false)
        }
        return ItemHolder(itemHolder!!)
    }

    override fun getItemCount(): Int {
        return arrayList!!.size
    }

    override fun onBindViewHolder(holder: MovieInformationAdapter.ItemHolder, position: Int) {
        val item = arrayList!![position]
        if (item!!.image != null) {

            if (item.image is Int) {
                holder.image.setImageResource(item.image)
            }
            else {
                val picasso = Picasso.get()
                picasso.load(item.image.toString()).into(holder.image)
            }
        }

        if (item.text != null){
            holder.text.text = item.text
        }

    }

    override fun getItemViewType(position: Int): Int {
        if (arrayList!![position]?.type == MovieInformationItemType.CellName)
            return 1
        else if (arrayList!![position]?.type == MovieInformationItemType.MovieCover)
            return 2
        else
            return 3
    }

    class ItemHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        val text = itemView.informationName
        val image = itemView.informationImage
    }
}